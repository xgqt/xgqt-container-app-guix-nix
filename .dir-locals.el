;; -*- no-byte-compile: t -*-

;; Directory Local Variables
;; For more information see (info "(emacs) Directory Variables")

((find-file . ((require-final-newline . t)
               (show-trailing-whitespace . t))))
