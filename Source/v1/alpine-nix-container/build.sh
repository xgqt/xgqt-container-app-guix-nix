#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e

script_path="${0}"
cd "$(dirname "${script_path}")"
script_directory="$(pwd)"

cd "${script_directory}"

docker build --file Containerfile -t "$(basename "${script_directory}")" "${script_directory}"
